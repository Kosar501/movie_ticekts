import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/pages/Home.vue";
import Auth from "../views/pages/Auth.vue";
import Tickets from "../views/pages/Tickets";
import Reserved from "../views/pages/Reserved";
Vue.use(VueRouter);
const routes = [
  {
    path: "/auth",
    name: "Auth",
    component: Auth,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: { requiresAuth: true },
  },
  {
    path: "/movie/:id/tickets",
    name: "Tickets",
    component: Tickets,
    meta: { requiresAuth: true },
  },
  {
    path: "/reserved",
    name: "Reserved",
    component: Reserved,
    meta: { requiresAuth: true },
  },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue"),
  // },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !localStorage.getItem("token")) {
    next({
      name: "Auth",
    });
  } else {
    next();
  }
});
export default router;
