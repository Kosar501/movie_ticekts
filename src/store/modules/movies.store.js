export default {
  state: {
    movies: [
      {
        id: 1,
        cover:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQWQBVC9wMklT_J_1sCzk03O0s4TdiZ-Ee1Lvi4oDGdKAzHCKvsFcmzhgsZS1WDS3Bhrk&usqp=CAU",
        title: "Children of heaven",
        publishYear: "1997",
        releaseDate: "2018/4/20",
        releaseTime: "22:00",
      },
      {
        id: 2,
        cover:
          "https://kanopy.com/cdn-cgi/image/fit=cover,height=900,width=1600/https://static-assets.kanopy.com/video-images/62b22086-8e12-417d-aaed-98d383b40c4c.jpg",
        title: "About Elly",
        publishYear: "2009",
        releaseDate: "2018/4/20",
        releaseTime: "22:00",
      },
      {
        id: 3,
        cover:
          "https://c8.alamy.com/comp/DT5H0B/leila-hatami-peyman-moaadi-poster-a-separation-2011-DT5H0B.jpg",
        title: "A separation",
        publishYear: "2009",
        releaseDate: "2018/4/20",
        releaseTime: "18:00",
      },
      {
        id: 4,
        cover:
          "https://mediaindia.eu/wp-content/uploads/2017/03/The_Salesman_releasing_india.jpg",
        title: "The salesman",
        publishYear: "2011",
        releaseDate: "2018/4/21",
        releaseTime: "18:00",
      },
      {
        id: 5,
        cover: "https://yasour.org/2016/uploads/5aa18f8d2f20c.jpg",
        title: "The Elephant king",
        publishYear: "2017",
        releaseDate: "2018/4/21",
        releaseTime: "20:00",
      },
    ],
  },
  getters: {
    getMovies(state) {
      return state.movies;
    },
  },
  mutations: {
    setMovies(state, data) {
      state.movies = data;
    },
  },
  actions: {
    getMoviesReq(context) {
      return new Promise(function (resolve) {
        resolve(context.getters.getMovies);
      });
    },
  },
};
