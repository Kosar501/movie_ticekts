export default {
  state: {
    reserved: [],
  },
  getters: {
    getReserved(state) {
      return state.reserved;
    },
    getReservedSeat(state) {
      return (movieId, ticket) => {
        let res = state.reserved.find(
          (obj) => obj.movieId === movieId && obj.ticket === ticket
        );
        if (res) {
          return true;
        }
        return false;
      };
    },
  },
  mutations: {
    setReservedEmpty(state) {
      state.reserved = [];
    },
    setReservedSeat(state, seat) {
      state.reserved.push(seat);
    },
  },
  actions: {
    reserve(context, { seat }) {
      return new Promise(function (resolve) {
        //check if seat is taken
        let res = context.getters.getReserved.find(
          (obj) => obj.movieId === seat.movieId && obj.ticket === seat.ticket
        );
        if (res) {
          return resolve(false);
        }
        //push to reserved tickets
        context.commit("setReservedSeat", seat);
        return resolve(true);
      });
    },
    // isMovieTicketReserved(context, { movieId, ticket }) {
    //     //check if seat is taken
    //     let res = context.getters.getReserved.find(
    //         (obj) => obj.movieId === movieId && obj.ticket === ticket);
    //     if(res) {
    //         return true;
    //     }
    //     return false;
    // },
  },
};
