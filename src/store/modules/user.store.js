export default {
  state: {
    user: {},
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
  mutations: {
    setUser(state, data) {
      state.user = data;
    },
  },
  actions: {
    login(context, { phone, password }) {
      return new Promise(function (resolve) {
        //commit user
        context.commit("setUser", {
          phone: phone,
          password: password,
        });
        //generate token and commit
        localStorage.setItem("token", Math.random().toString(36).substr(2, 30));
        return resolve(true);
      });
    },
    logout() {
      return new Promise(function (resolve) {
        //generate token and commit
        localStorage.removeItem("token");
        return resolve(true);
      });
    },
  },
};
