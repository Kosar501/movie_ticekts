import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
//modules
import movies from "./modules/movies.store";
import user from "./modules/user.store";
import tickets from "./modules/tickets.store";
Vue.use(Vuex);
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    movies,
    tickets,
  },
  plugins: [createPersistedState()],
});
